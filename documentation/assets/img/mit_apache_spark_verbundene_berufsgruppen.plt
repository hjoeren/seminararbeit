set term svg font "sans-serif"
set yrange [0:60]
set datafile separator ','
set output '..\img\mit_apache_spark_verbundene_berufsgruppen.svg'
set title "{/:Bold Mit Apache Spark verbundene Berufsgruppen}"
set xlabel 'Berufsgruppe'
set ylabel 'Prozent'
set key off
set xtics nomirror 
set xtics rotate by -45
set ytics nomirror
set style data boxes
set boxwidth 0.75
set style fill solid
plot '..\data\mit_apache_spark_verbundene_berufsgruppen.csv' using 0:($2*100):xticlabels(1) with boxes, '' using 0:($2*100):(sprintf("%.2f%",($2*100))) with labels center offset 0,1