set term svg font "sans-serif"
set timefmt "%Y-%m-%d - %Y-%m-%d"
set xdata time
set xrange ["2009-12-20 - 2009-12-26":"2015-12-27 - 2016-01-02"]
set format x "%Y"
set datafile separator ','
set output '..\img\websuche-interesse_von_apache_spark_und_apache_hadoop.svg'
set title "{/:Bold Websuche-Interesse von Apache Spark und Apache Hadoop}"
set xlabel 'Zeit (Jahr)'
set ylabel 'Interesse (Prozent)'
set key top vertical left box
set xtics nomirror
set ytics nomirror
plot '..\data\websuche-interesse_von_apache_spark_und_apache_hadoop.csv' using 1:2 with lines title columnhead, '' using 1:3 with lines title columnhead