set term svg font "sans-serif"
set yrange [0:40]
set datafile separator ','
set output '..\img\bedeutendste_branchen_mit_einsatz_von_apache_spark.svg'
set title "{/:Bold Bedeutendste Branchen mit Einsatz von Apache Spark}"
set xlabel 'Branche'
set ylabel 'Prozent'
set key off
set xtics nomirror 
set xtics rotate by -45
set ytics nomirror
set style data boxes
set boxwidth 0.75
set style fill solid
plot '..\data\bedeutendste_branchen_mit_einsatz_von_apache_spark.csv' using 0:($2*100):xticlabels(1) with boxes, '' using 0:($2*100):(sprintf("%.2f%",($2*100))) with labels center offset 0,1