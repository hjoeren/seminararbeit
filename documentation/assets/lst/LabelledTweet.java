package de.hska.iwii.bigdata.spark;

public class LabelledTweet extends Tweet {
  
  private double label;

  public LabelledTweet(String text, double label) {
    super(text);
    this.label = label;
  }

  public LabelledTweet(String text) {
    this(text, 0);
  }

  public double getLabel() {
    return this.label;
  }

  public void setLabel(double label) {
    this.label = label;
  }
  
}
