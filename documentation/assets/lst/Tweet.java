package de.hska.iwii.bigdata.spark;

import java.io.Serializable;

public class Tweet implements Serializable {
  
  private String text;

  public Tweet(String text) {
    this.text = text;
  }

  public String getText() {
    return this.text;
  }

  public void setText(String text) {
    this.text = text;
  }
  
}
