package de.hska.iwii.bigdata.spark;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.apache.commons.lang3.ArrayUtils;
import org.apache.commons.lang3.StringUtils;
import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaPairRDD;
import org.apache.spark.api.java.JavaRDD;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.ml.Pipeline;
import org.apache.spark.ml.PipelineModel;
import org.apache.spark.ml.PipelineStage;
import org.apache.spark.ml.Transformer;
import org.apache.spark.ml.classification.LogisticRegression;
import org.apache.spark.ml.evaluation.BinaryClassificationEvaluator;
import org.apache.spark.ml.feature.HashingTF;
import org.apache.spark.ml.feature.Tokenizer;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;
import org.apache.spark.streaming.Durations;
import org.apache.spark.streaming.api.java.JavaDStream;
import org.apache.spark.streaming.api.java.JavaReceiverInputDStream;
import org.apache.spark.streaming.api.java.JavaStreamingContext;
import org.apache.spark.streaming.twitter.TwitterUtils;

import scala.Tuple2;
import twitter4j.Status;
import twitter4j.auth.Authorization;
import twitter4j.auth.AuthorizationFactory;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;

public class TwitterSentimentAnalysis {

  private static ClassLoader classLoader = TwitterSentimentAnalysis.class.getClassLoader();

  private static String sparkAppName;
  private static String sparkHome;
  private static String sparkMaster;
  private static String sparkLogLevel;

  private static boolean twitterDebug;
  private static String twitterConsumerKey;
  private static String twitterConsumerSecret;
  private static String twitterAccessToken;
  private static String twitterAccessTokenSecret;
  private static String[] twitterFilters;
  private static String[] twitterLangs;

  private static String inputPath;
  private static String outputPath;

  private static Properties loadProperties(InputStream propertiesStream) {
    try {
      Properties properties = new Properties();
      properties.load(propertiesStream);
      return properties;
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  private static Properties loadProperties(String propertiesPath) {
    try {
      InputStream propertiesStream = new FileInputStream(propertiesPath);
      return loadProperties(propertiesStream);
    } catch (IOException e) {
      e.printStackTrace();
      throw new RuntimeException();
    }
  }

  private static Properties loadDefaultProperties() {
    InputStream propertiesStream = classLoader.getResourceAsStream("application.properties");
    return loadProperties(propertiesStream);
  }

  private static void initializeApplicationConfiguration(String[] args) {
    Properties properties = args.length == 0 ? loadDefaultProperties() : loadProperties(args[0]);

    sparkAppName = properties.getProperty("spark.app.name");
    sparkHome = properties.getProperty("spark.home");
    sparkMaster = properties.getProperty("spark.master");
    sparkLogLevel = properties.getProperty("spark.log.level");

    twitterDebug = Boolean.parseBoolean(properties.get("twitter.debug").toString());
    twitterConsumerKey = properties.getProperty("twitter.consumerKey");
    twitterConsumerSecret = properties.getProperty("twitter.consumerSecret");
    twitterAccessToken = properties.getProperty("twitter.accessToken");
    twitterAccessTokenSecret = properties.getProperty("twitter.accessTokenSecret");
    twitterFilters = StringUtils.split(StringUtils.lowerCase(properties.getProperty("twitter.filters")), ",");
    twitterLangs = StringUtils.split(StringUtils.lowerCase(properties.getProperty("twitter.langs")), ",");

    inputPath = properties.getProperty("input.path");
    outputPath = properties.getProperty("output.path");

    System.out.println("Spark App Name:\t\t\t" + sparkAppName);
    System.out.println("Spark Home:\t\t\t" + sparkHome);
    System.out.println("Spark Master:\t\t\t" + sparkMaster);
    System.out.println("Spark Log Level:\t\t" + sparkLogLevel);
    System.out.println("Twitter Debug:\t\t\t" + twitterDebug);
    System.out.println("Twitter Consumer Key:\t\t" + StringUtils.replacePattern(twitterConsumerKey, ".", "*"));
    System.out.println("Twitter Consumer Secret:\t" + StringUtils.replacePattern(twitterConsumerSecret, ".", "*"));
    System.out.println("Twitter Access Token:\t\t" + StringUtils.replacePattern(twitterAccessToken, ".", "*"));
    System.out.println("Twitter Access Token Secret:\t" + StringUtils.replacePattern(twitterAccessTokenSecret, ".",
        "*"));
    System.out.print("Twitter Filters:\t\t");
    for (String filter : twitterFilters) {
      System.out.print(filter + " ");
    }
    System.out.println();
    System.out.print("Titter Langs:\t\t\t");
    for (String lang : twitterLangs) {
      System.out.print(lang + " ");
    }
    System.out.println();
    System.out.println("Input Path:\t\t\t" + inputPath);
    System.out.println("Output Path:\t\t\t" + outputPath);
  }

  private static Transformer getOrCreateModel(JavaSparkContext sparkContext, SQLContext sqlContext) {
    try {
      return PipelineModel.load(outputPath + "/model");
    } catch (Exception ex) {
    }

    JavaRDD<String> corpus = sparkContext.textFile(inputPath + "/corpus/*");
    corpus = corpus.map(line -> StringUtils.removeStart(line, "\""));
    corpus = corpus.map(line -> StringUtils.removeEnd(line, "\""));
    corpus = corpus.map(line -> StringUtils.replace(line, "\",\"", "\t"));

    JavaRDD<LabelledTweet> parsedCorpus = corpus.map(line -> {
      String[] columns = line.split("\\t");
      String text = columns[5];
      int label;
      switch (columns[0]) {
        case "0":
          label = 0;
          break;
        case "4":
          label = 1;
          break;
        default:
          label = -1;
      }
      return new LabelledTweet(text, label);
    });

    DataFrame labelledTweets = sqlContext.createDataFrame(parsedCorpus, LabelledTweet.class);
    double[] weights = {0.8, 0.2};
    DataFrame[] data = labelledTweets.randomSplit(weights);
    DataFrame trainingData = data[0];
    DataFrame testData = data[1];

    Tokenizer tokenizer = new Tokenizer();
    tokenizer.setInputCol("text");
    tokenizer.setOutputCol("words");

    HashingTF hashingTF = new HashingTF();
    hashingTF.setNumFeatures(100000);
    hashingTF.setInputCol(tokenizer.getOutputCol());
    hashingTF.setOutputCol("features");

    LogisticRegression logisticRegression = new LogisticRegression();
    logisticRegression.setMaxIter(1000);
    logisticRegression.setRegParam(0.0001);

    PipelineStage[] stages = {tokenizer, hashingTF, logisticRegression};
    Pipeline pipeline = new Pipeline();
    pipeline.setStages(stages);

    PipelineModel pipelineModel = pipeline.fit(trainingData);
    DataFrame testPredictions = pipelineModel.transform(testData);
    DataFrame trainingPredictions = pipelineModel.transform(trainingData);

    BinaryClassificationEvaluator evaluator = new BinaryClassificationEvaluator();
    evaluator.setMetricName("areaUnderROC");

    double trainingMetrics = evaluator.evaluate(trainingPredictions);
    double testMetrics = evaluator.evaluate(testPredictions);

    System.out.println(trainingMetrics);
    System.out.println(testMetrics);

    try {
      pipelineModel.save(outputPath + "/model");
    } catch (IOException ex) {
      System.err.println("Can't save model: " + ex.getMessage());
    }

    return pipelineModel;
  }

  private static JavaDStream<Tweet> streamTweets(JavaSparkContext sparkContext, JavaStreamingContext streamingContext) {
    ConfigurationBuilder twitterConfigurationBuilder = new ConfigurationBuilder();
    twitterConfigurationBuilder.setOAuthConsumerKey(twitterConsumerKey);
    twitterConfigurationBuilder.setOAuthConsumerSecret(twitterConsumerSecret);
    twitterConfigurationBuilder.setOAuthAccessToken(twitterAccessToken);
    twitterConfigurationBuilder.setOAuthAccessTokenSecret(twitterAccessTokenSecret);
    Configuration twitterConfiguration = twitterConfigurationBuilder.build();
    Authorization twitterAuthorization = AuthorizationFactory.getInstance(twitterConfiguration);

    JavaReceiverInputDStream<Status> statusStream = TwitterUtils.createStream(streamingContext, twitterAuthorization,
        twitterFilters);
    JavaDStream<Status> tweetStream = statusStream.filter(status -> !status.isRetweet());
    tweetStream = tweetStream.filter(status -> ArrayUtils.contains(twitterLangs, status.getLang().toLowerCase()));

    JavaDStream<Tweet> parsedTweetStream = tweetStream.map(tweet -> new Tweet(StringUtils.strip(tweet.getText())));

    return parsedTweetStream;
  }

  private static void analyzeTweets(JavaSparkContext sparkContext, SQLContext sqlContext, Transformer model,
      JavaDStream<Tweet> tweets) {
    tweets.foreachRDD((rdd, time) -> {
      DataFrame unlabelledTweet = sqlContext.createDataFrame(rdd, Tweet.class);
      JavaPairRDD<String, Double> labelledTweet = model.transform(unlabelledTweet)
          .select("text", "prediction")
          .javaRDD()
          .mapToPair(tweet -> new Tuple2<String, Double>(tweet.getString(0), tweet.getDouble(1)));
      labelledTweet.saveAsTextFile(outputPath + "/tweets/tweets_" + time.milliseconds() + ".txt");
    });
  }

  public static void main(String[] args) {
    initializeApplicationConfiguration(args);

    SparkConf sparkConf = new SparkConf();
    sparkConf.setAppName(sparkAppName);
    sparkConf.setSparkHome(sparkHome);
    sparkConf.setMaster(sparkMaster);

    JavaSparkContext sparkContext = new JavaSparkContext(sparkConf);
    sparkContext.setLogLevel(sparkLogLevel);

    SQLContext sqlContext = new SQLContext(sparkContext);
    JavaStreamingContext streamingContext = new JavaStreamingContext(sparkContext, Durations.seconds(5));

    Transformer model = getOrCreateModel(sparkContext, sqlContext);

    JavaDStream<Tweet> tweetStream = streamTweets(sparkContext, streamingContext);

    analyzeTweets(sparkContext, sqlContext, model, tweetStream);

    streamingContext.start();
    streamingContext.awaitTerminationOrTimeout(Durations.minutes(360).milliseconds());
  }


}
