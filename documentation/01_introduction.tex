\chapter{Einleitung}
\label{kap:einleitung}

Im folgenden Kapitel wird eine Einleitung in das Thema \enquote{Spark} bzw. in das übergeordnete Themenkomplex, in welches sich Spark einordnen lässt, gegeben. Dabei wird eine Definition bzw. für diese Arbeit passende Erklärung des Begriffs \enquote{Big Data} gegeben. Anschließend werden Werkzeuge zur Auswertung und Analyse von Big Data aufgelistet und abschließend die Motivation zur Auswahl des Themas \enquote{Spark} vorgestellt.

\section{Der Begriff Big Data}
\label{sek:der_begriff_big_data}

Laut einer Statistik von \citeauthor{CiscoSystems2015} \cite{CiscoSystems2015} betrug das monatliche Datenvolumen des privaten und geschäftlichen IP-Traffics im Jahr 2014 $59.851$ Petabyte und soll sich bis zum Jahr 2019 auf $167.973$ Petabyte fast verdreifachen. Bei Daten dieser Größenordnung ist es nicht verwunderlich, dass der Begriff \enquote{Big Data} fällt.

Doch Big Data umfasst mehr Eigenschaften, als nur eine riesige Datenmenge. So beschreibt \citeauthor{Beyer2011} neben einem großen Volumen (engl. \textit{volume}) noch zwei weitere Eigenschaften, die Big Data hat: große Vielfalt (engl. \textit{variety}) und hohe Geschwindigkeit (engl. \textit{velocity}) \cite{Beyer2011}. Hieraus ergibt sich das sogenannte $3$-V-Modell, welches in \autoref{abb:3_v_modell} abgebildet ist.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{3-v-modell}
	\caption[$3$-V-Modell für Big Data]{$3$-V-Modell für Big Data \cite[in Anlehnung an:][Abb.~1]{KleinEtAl2013}}
	\label{abb:3_v_modell}
\end{figure}

\paragraph{Volume} Der rasche Anstieg des Datenvolumens, welcher unter anderem aufgrund von sozialen Netzwerken wie Facebook oder Twitter verursacht wird, stellt neben eines Speicherproblems zusätzlich noch ein Problem bezüglich der Datenanalyse dar.
\paragraph{Variety} Gleichzeitig mit dem Anstieg des Datenvolumens stellt auch die Datenvielfalt ein Problem dar. So müssen neben herkömmlichen strukturierten Daten auch semi- und unstrukturierte Daten analysiert werden. Die Datenvielfalt umfasst tabellarische Daten (Datenbanken), hierarchische Daten, Dokumente, E-Mails, Meta-Daten, Videodaten, Bilddaten, Audio-Daten, Börsendaten, Finanztransaktionen und viele mehr.
\paragraph{Velocity} Mit der Datengeschwindigkeit wird einerseits angesprochen, wie schnell die Daten erzeugt werden, und andererseits, wie schnell die Daten verarbeitet werden müssen.

Das $3$-V-Modell von \citeauthor{Beyer2011} wird besonders von \citelist{Kobielus2013}{organization} um eine weitere Eigenschaft, die Richtigkeit (engl. \textit{veracity}), ergänzt \cite{Kobielus2013}.

%\begin{description}
	\paragraph{Veracity} Aufgrund der zunehmenden Datenquellen steigt auch die Ungenauigkeit bzw. die Richtigkeit der aufkommenden Daten.
%\end{description}

Darüber hinaus nennen einige weitere Quellen \parencite{DemchenkoEtAl2013, AssuncaoEtAl2015} weitere Eigenschaften, darunter ein fünftes V für den Wert (engl. \textit{value}), auf welche jedoch in dieser Arbeit nicht eingegangen wird. Somit genügt für diese Arbeit die folgende Beschreibung: Bei Big Data handelt es sich um Daten mit einem hohen Volumen, welche unterschiedlich strukturiert sind, schnell erzeugt und abgeändert werden, schnell verarbeitet werden müssen und sich in ihrer Richtigkeit unterscheiden.

\section{Big Data Technologien}
\label{sek:big_data_technologien}

Um die Probleme bzw. Herausforderungen, welche durch die in \autoref{sek:der_begriff_big_data} genannten Eigenschaften von Big Data verursacht werden, zu bewältigen und solche Daten zu analysieren, sind in naher Vergangenheit zahlreiche Technologien entstanden und sind aktuell dabei zu entstehen. Hierbei eignen sich die einzelnen Technologien meist nicht für den gesamten Themenkomplex Big Data, sondern beziehen sich lediglich auf eine oder mehrere Eigenschaften, die einzelnen V's. Eine genaue Erläuterung aller Technologien würde den Rahmen dieser Arbeit sprengen, weshalb im Folgenden nur eine kleine Auswahl erwähnt und kurz beschrieben wird. 

\subsection*{Hadoop}
\label{subsek:hadoop}

Eine der bekanntesten Technologien in Bezug auf Big Data ist Hadoop\footnote{\url{http://hadoop.apache.org/}}, ein Framework der Apache Software Foundation. Im Grunde genommen handelt es sich hierbei nicht um eine, sondern um mehrere Technologien. Es besteht aus drei Modulen: \acrshort{hdfs}, Hadoop MapReduce und Hadoop \acrshort{yarn}.

\subsubsection{\acrshort{hdfs}}
\label{subsubsek:hdfs}

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{hdfs-architektur.eps}
	\caption[\acrshort{hdfs}-Architektur]{\acrshort{hdfs}-Architektur \cite[in Anlehnung an:][]{ASFnodatec}}
	\label{abb:hdfs_architektur}
\end{figure}

\gls{hdfs} ist ein verteiltes Dateisystem. Es besteht aus Arten von Knoten, den sogenannten Name- und DataNodes (enlg. für \textit{Namens-} und \textit{Datenknoten}). Dabei werden die Dateien in Metadaten und Dateiblöcke bestimmter Größe zerlegt. Die einzelnen Datenblöcke von Dateien liegen redundant auf unterschiedlichen DataNodes. Der NameNode verwaltet dagegen die Metadaten der Dateien. Hierzu gehören die Namen und Zugriffsrechte der Dateien, aber auch die Standorte, also Referenzen auf die DataNodes, auf welchen die einzelnen Datenblöcke der Dateien liegen. \autoref{abb:hdfs_architektur} zeigt die \gls{hdfs}-Architektur. Für weitere Informationen sei auf die \gls{hdfs}-Dokumentation \cite{ASFnodatec} verwiesen.

\subsubsection{Hadoop MapReduce}
\label{subsubsek:hadoop_mapreduce}

Bei MapReduce handelt es sich um ein Programmiermodell, welches eine Aufgabe in mehrere Teilaufgaben aufteilt (Map-Prozess), diese auf mehrere Rechnerknoten zur parallelen Bearbeitung verteilt, Zwischenergebnisse zwischen den einzelnen Rechnerknoten austauscht (Shuffle-Prozess) und anschließend die Zwischenergebnisse zu einem Endergebnis zusammenfasst (Reduce-Prozess) \cite{FelsEtAl2015}. Für ausführlichere Informationen sei auf den Artikel \citetitle{DeanEtAl2008} von \citeauthor{DeanEtAl2008} verwiesen \cite{DeanEtAl2008}.

\subsubsection{Hadoop \acrshort{yarn}}
\label{subsubsek:hadoop_yarn}

\gls{yarn} ist seit Version 2 Bestandteil von Hadoop. Zuvor war Hadoop ein stark gekoppeltes System und erlaubte es unter anderem nur, das eigene MapReduce-Verfahren einzusetzen. Mithilfe von \gls{yarn} ist es nun möglich, eine Reihe anderer Verfahren einzusetzen. Dies wird durch eine Verteilung der Aufgaben, welche in Version 1 in der MapReduce-Komponente erledigt wurden, ermöglicht. Die Architektur von \gls{yarn} ist in \autoref{abb:yarn_architektur} dargestellt. Eine genaue Beschreibung ist in der \gls{yarn}-Dokumentation der \citeauthor{ASFnodatee} gegeben \cite{ASFnodatee}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{yarn-architektur.eps}
	\caption[\acrshort{yarn}-Architektur]{\acrshort{yarn}-Architektur \cite[in Anlehnung an:][]{ASFnodated}}
	\label{abb:yarn_architektur}
\end{figure}

\section{Motivation}
\label{sek:motivation}

Trotz der bereits zahlreich vorhandenen Technologien für Big Data, worunter Hadoop hervorzuheben ist, wird Apache Spark immer beliebter. In \autoref{abb:interesse_im_zeitlichen_verlauf} sind die Suchvorgänge der vergangenen Jahre von den Begriffen \enquote{Apache Spark} und \enquote{Apache Hadoop} auf der Suchmaschine Google gegenübergestellt. Hieraus lässt sich andeutungsweise erkennen, dass zumindest ein tieferes Interesse an dem Framework besteht. So verzeichnet auch eine Umfrage von \citeauthor{Databricks2015a} einen rapiden Anstieg der Spark-Benutzer vom Jahr 2014 auf 2015 \cite{Databricks2015a}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{websuche-interesse_von_apache_spark_und_apache_hadoop.eps}
	\caption[Websuche-Interesse von Apache Spark und Apache Hadoop]{Websuche-Interesse von Apache Spark und Apache Hadoop\footnotemark}
	\label{abb:interesse_im_zeitlichen_verlauf}
\end{figure}

\footnotetext{Datenquelle: \href{https://www.google.com/trends/}{Google Trends}}

Des Weiteren wird in zahlreichen Blogs darüber diskutiert, ob Spark in naher Zukunft Hadoop verdrängt bzw. ersetzt und es fallen Aussagen wie \textcquote{Schoen2015}{Hadoop bekommt Konkurrenz: Apache Spark ist 100 mal schneller} oder es kommen die Fragen auf wie \textcquote{Mohammed2015}{Ersetzt Apache Spark Hadoop?}. Die folgende Arbeit beschäftigt sich nun näher mit dem Thema \enquote{Apache Spark}, beschreibt dessen Architektur und Komponenten und stellt die beiden Technologien Spark und Hadoop in einem Vergleich gegenüber, um grundlegende Vor- bzw. Nachteile von Spark aufzuzeigen und die genannten Aussagen zu stützen oder zu widerlegen.