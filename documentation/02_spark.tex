\chapter{Spark}

In diesem Kapitel wird Apache Spark vorgestellt. Dabei wird zuerst auf die Geschichte und allgemeine Aspekte des Frameworks eingegangen. Anschließend werden die einzelnen Komponenten näher beschrieben und abschließend auf die Einsatzfelder von Spark genannt.

\section{Geschichte}

Spark startete als Forschungsprojekt im AMPLab\footnote{\url{https://amplab.cs.berkeley.edu/}} der University of California, Berkeley, im Jahr 2009. Anfang 2010 wurden die Quellen des Projektes der Öffentlichkeit zugänglich gemacht und infolge dessen seit 2013 von der Apache Software Foundation geleitet. Derzeit beteiligen sich über 400 Entwickler von über 100 Unternehmen an der Entwicklung von Apache Spark \cite{ASFnodateb}.

\section{Allgemeines}
\label{sek:allgemeines}

Spark bezeichnet sich selbst als ein schnelles, skalierendes und für allgemeine Zwecke geeignetes Framework für die Verarbeitung und Analyse von Daten \cite{ASFnodatea}. Zu den Key-Features werden dabei folgende Punkte genannt:

\begin{itemize}
	\item Geschwindigkeit
	\item Einfachheit
	\item Allgemeinheit und 
	\item Unabhängigkeit.
\end{itemize}

Dabei wird angemerkt, dass es bis zu 100 mal schneller als Hadoop MapReduce bei der Verarbeitung von In-Memory-Daten und bis zu 10 mal schneller bei der Verarbeitung von Plattendaten sein soll.

Der Gesichtspunkt der Einfachheit wird durch das Angebot von \glspl{api} in unterschiedlichen Programmiersprachen gefördert. So ist es möglich, Spark-Applikationen mittels Java, Scala, Python und R zu erstellen. Das Framework selbst ist in Scala implementiert.

Die Allgemeinheit wird durch das Anbieten von unterschiedlichen Bibliotheken, welche sich unter anderem auf die einzelnen V's beziehen, gestützt. Die einzelnen Bibliotheken (Spark SQL, Spark Streaming, MLlib und GraphX) werden in \autoref{sek:komponenten} näher beschrieben.\footnote{Neben den erwähnten Bibliotheken aus dem Spark Stack werden noch weitere Bibliotheken angeboten, die das Programmieren von Applikationen erleichtern. Eine Übersicht ist auf \url{http://repo.maven.apache.org/maven2/org/apache/spark/} zu finden.}

Das letzte genannte Feature von Spark, die Unabhängigkeit, wird dadurch gewährleistet, dass Spark auf verschiedenen Plattformen aufsetzen kann. So kann Spark in den folgenden Modi betrieben werden:

\begin{itemize}
	\item im Standalone-Modus
	\item über Amazon EC2 (\url{https://aws.amazon.com/de/ec2/})
	\item über Hadoop \gls{yarn} oder
	\item über Apache Mesos (\url{http://mesos.apache.org/}).
\end{itemize}

Ein weiterer Punkt ist, dass Spark auf Daten von verschiedenen Quellen zugreifen kann. So werden die folgenden Systeme unterstützt:

\begin{itemize}
	\item \gls{hdfs}
	\item Apache Cassandra (\url{http://cassandra.apache.org/})
	\item Apache HBase (\url{http://hbase.apache.org/})
	\item Apache Hive (\url{http://hive.apache.org/}) und
	\item Alluxio (früher Tachyon) (\url{http://alluxio.org/})
\end{itemize}

\section{Komponenten}
\label{sek:komponenten}

Wie bereits im allgemeinen Teil erwähnt, bietet Spark mehrere, die in \autoref{abb:spark_stack} abgebildeten, Bibliotheken an, welche die einzelnen Komponenten darstellen und das gesamte Framework bilden. Hierbei spricht man von dem sogenannten \enquote{Spark Stack}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{spark-stack.eps}
	\caption[Spark Stack]{Spark Stack \cite[in Anlehnung an:][]{ASFnodatea}}
	\label{abb:spark_stack}
\end{figure}

All diese Bibliotheken lassen sich mit den in \autoref{sek:allgemeines} genannten Programmiersprachen ansprechen und einbinden. Hierbei wird sich im Folgenden, im Falle von Code-Beispielen, lediglich auf Java bezogen. 

\subsection{Spark Core}
\label{sek:spark_core}

Spark Core ist die Basis des gesamten Spark Frameworks. Es stellt sowohl die Infrastruktur, inklusive deren Verwaltung, als auch die Funktionen zur Erstellung von Applikationen für die Verarbeitung von Daten bereit.

\subsubsection{High-Level-Architektur}

Die Architektur eines Spark-Systems besteht aus fünf Entitäten, welche in \autoref{abb:high_level_architektur} dargestellt sind \cite{Guller2015}.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.5\textwidth]{high-level-architektur.eps}
	\caption[High-Level-Architektur]{High-Level-Architektur \cite[in Anlehnung an:][\figureautorefname~3.1]{Guller2015}}
	\label{abb:high_level_architektur}
\end{figure}

\paragraph{Worker Node} Ein Worker Node (engl. \textit{Arbeiterknoten}) ist eine physikalische Einheit und stellt einen Rechner in einem Cluster dar. Er stellt explizit die von einer Spark-Applikation benötigten Ressourcen (CPU, Speicher, etc.) zur Verfügung.  
\paragraph{Cluster Manager} Ein Cluster Manager ist dafür verantwortlich, die gesamten Ressourcen in einem Cluster, also aller im Cluster befindlichen Knoten, zu verwalten und diese Ressourcen zu verteilten. Bei Spark können hierbei verschiedene Typen von Cluster Managern eingesetzt werden. So z. B. ein eigener Standalone-Manager, Hadoop \gls{yarn} oder Apache Mesos.
\paragraph{Driver Program} Ein Driver Program ist das auf Spark zu laufende Programm, welches den Code, der im Cluster ausgeführt werden soll, beinhaltet. Es verwendet die einzelnen Bibliotheken (insbesondere die Core-Bibliothek) von Spark. In dieser Arbeit wird hierbei von der Spark-Applikation gesprochen. Wie eine Spark-Applikation in Java genau initialisiert wird, ist in \autoref{lst:initialisierung_einer_spark_applikation} beispielhaft zu sehen.
\paragraph{Executor} Ein Executor (engl. \textit{Ausführender}) ist eine \gls{jvm}, die für jede Spark-Applikation auf einem Knoten im Cluster gestartet wird und solange aktiv wie dieselbe ist. Diese \gls{jvm} ist für die Abarbeitung des Codes der Spark-Applikation verantwortlich.
\paragraph{Task} Ein Task (engl. \textit{Aufgabe}) ist die kleinste Einheit in der Architektur. Er stellt die einzelnen Aufgaben dar, die auf einem Executor, also in einer \gls{jvm}, ausgeführt werden.

\begin{lstlisting}[language=Java, numbers=left, caption={Initialisierung einer Spark-Applikation}, label={lst:initialisierung_einer_spark_applikation}, float=ht]
public static void main(String[] args) {
	SparkConf configuration = new SparkConf();
	configuration.setAppName("Beispiel");	// Name der Applikation
	configuration.setMaster("local[*]");	// Adresse des Cluster Managers
	JavaSparkContext context = new JavaSparkContext(configuration);	
	
	// ...
}
\end{lstlisting}

\subsubsection{\acrshort{rdd}}

\glspl{rdd} (engl. \textit{elastisch verteilte Datensätze}) sind Datenstrukturen vergleichbar mit Arrays oder Listen. Sie ermöglichen die parallele Verarbeitung der beinhaltenden Daten. \glspl{rdd} werden in der Spark-Applikation erzeugt. Dabei können sie innerhalb einer Applikation oder automatisch aus bestehenden Datenquellen (z. B. aus einem \gls{hdfs}) erzeugt bzw. generiert werden (siehe \autoref{lst:erzeugung_von_rdds}).

\begin{lstlisting}[language=Java, numbers=left, caption={Erzeugung von RDDs}, label={lst:erzeugung_von_rdds}, float=ht]
// RDD, welches innerhalb einer Applikation erzeugt wird
String[] data = new String[]{"eins", "zwei", "drei", "vier", "fuenf"};
JavaRDD<String> rdd1 = context.parallelize(Arrays.asList(data));

// RDD, welches aus Daten eines HDFS generiert wird
JavaRDD<String> rdd2 = context.textFile("hdfs://localhost:9000/data");  
\end{lstlisting}

Neben der Möglichkeit nur einfache Werte in einem \gls{rdd} zu speichern, ist es auch möglich, Schlüssel-Werte-Paare in \glspl{rdd} zu speichern. \autoref{lst:erzeugung_eines_rdds_mit_s_w_paaren} veranschaulicht die Erzeugung eines Datensatzes, welcher solche Schlüssel-Werte-Paare beinhaltet. Dabei wird eine Funktion bzw. Transformation \lstinline|mapToPair(..)| verwendet, welche später erläutert wird.

\begin{lstlisting}[language=Java, numbers=left, caption={Erzeugung eines RDDs mit Schlüssel-Werte-Paaren}, label={lst:erzeugung_eines_rdds_mit_s_w_paaren}, float=ht]
String[] data = new String[]{
	"Karl: 5", "Heinz: 8", "Karl: 3", "Fritz: 2", "Heinz: 1", "Peter: 9"
};
JavaRDD<String> singleRDD = context.parallelize(Arrays.asList(data));
JavaPairRDD<String, Integer> = singleRDD.mapToPair(line -> {	
	String[] columns = line.split(":");	// Teilen der Zeile
	String key = columns[0];	// Extrahieren der ersten Spalte
	String value = columns[1];	// Extrahieren der zweiten Spalte
	return new Tuple2<String, Integer>(key, Integer.parseInt(value);
});
\end{lstlisting}

\paragraph{Partitionierung} Die Datensätze bzw. \glspl{rdd} werden in mehrere Partitionen zerlegt. Pro Partition wird für die parallele Verarbeitung ein einzelner Task erzeugt, welcher dann auf einem Knoten im Cluster abgearbeitet wird. Die Anzahl an Partitionen hängt hierbei entweder von dem Programmierer der Spark-Applikation oder von der Konfiguration des Clusters ab.

\paragraph{Transformationen} Transformationen sind Funktionen, die auf Datensätze angewendet werden und aus einem Datensatz einen neuen Datensatz erzeugen. Dabei kann der Zieldatensatz unterschiedliche Strukturen aufweisen, andere Werte oder aber auch andere Schlüssel als der Ursprungsdatensatz besitzen. \autoref{tab:auf_rdds_anwendbare_transformationen} zeigt einen Ausschnitt der anwendbaren Transformationen und erläutert diese annähernd.

\begin{table}[ht]
	\caption[Auf \acrshortpl{rdd} anwendbare Transformationen]{Auf \acrshortpl{rdd} anwendbare Transformationen \cite[Auszug aus:][]{ASFnodatef}}
	\label{tab:auf_rdds_anwendbare_transformationen}
	\begin{tabularx}{\textwidth}{lX}
		\toprule[1.5pt]
		\textbf{Transformation} & \textbf{Bedeutung} \\
		\toprule[1.5pt]
		\lstinline|map| & Erzeugt einen neuen Datensatz, wobei auf jedes Element eine Funktion angewendet wird.\\
		\midrule
		\lstinline|filter| & Erzeugt einen neuen Datensatz, wobei auf jedes Element eine Vergleichsfunktion angewendet wird.\\
		\midrule
		\lstinline|flatMap| & Ähnlich wie bei \lstinline|map|, nur dass jedes Eingangselement auf 0 oder mehrere Ausgangselemente abgebildet werden kann.\\
		\midrule
		\lstinline|union| & Erzeugt einen neuen Datensatz, der die Vereinigung des Ursprungsdatensatzes und des Arguments beinhaltet.\\
		\midrule
		\lstinline|intersection| & Erzeugt einen neuen Datensatz, der die Schnittmenge des Ursprungsdatensatzes und des Arguments beinhaltet.\\
		\midrule
		\lstinline|groupByKey| & Erzeugt einen neuen Datensatz, wobei die Schlüssel gruppiert sind und die Werte aus Listen von den Ursprungswerten der dazugehörigen Schlüssel bestehen.\\
		\midrule
		\lstinline|reduceByKey| & Erzeugt einen neuen Datensatz, wobei die Schlüssel gruppiert sind und die Werte aggregiert sind.\\
		\midrule
		\lstinline|sortByKey| & Erzeugt einen neuen Datensatz, wobei die Schlüssel sortiert sind.\\
		\bottomrule[1.25pt]
	\end{tabularx}
\end{table} 

\paragraph{Aktionen} Aktionen sind Funktionen, die ebenfalls auf Datensätze angewendet werden, jedoch anders als bei Transformationen keinen neuen Datensatz erzeugen. Sie erzeugen bestimmte Ergebnisse oder arbeiten nicht-modifizierend auf den Elementen der Datensätze. Eine Auswahl an möglichen Aktionen ist in \autoref{tab:auf_rdds_anwendbare_aktionen} dargestellt und ebenfalls kurz beschrieben.

\begin{table}[ht]
	\caption[Auf \acrshortpl{rdd} anwendbare Aktionen]{Auf \acrshortpl{rdd} anwendbare Aktionen \cite[Auszug aus:][]{ASFnodatef}}
	\label{tab:auf_rdds_anwendbare_aktionen}
	\begin{tabularx}{\textwidth}{lX}
		\toprule[1.5pt]
		\textbf{Aktion} & \textbf{Bedeutung} \\
		\toprule[1.5pt]
		\lstinline|reduce| & Aggregiert die Elemente des Datensatzes, wobei auf jedes Element eine Funktion angewendet wird.\\
		\midrule
		\lstinline|collect| & Erzeugt eine Liste bzw. ein Array aus allen Elementen eines Datensatzes.\\
		\midrule
		\lstinline|count| & Ermittelt die Anzahl der Elemente eines Datensatzes.\\
		\midrule
		\lstinline|first| & Ermittelt das erste Element eines Datensatzes.\\
		\midrule
		\lstinline|saveAsTextFile| & Speichert den Datensatz als Textdatei.\\
		\midrule
		\lstinline|saveAsObjectFile| & Speichert den Datensatz als Objektdatei.\\
		\midrule
		\lstinline|countByKey| & Ermittelt die Anzahl von jedem vorkommenden Schlüssel eines Datensatzes.\\
		\midrule
		\lstinline|foreach| & Führt eine Funktion auf jedes Element eines Datensatzes aus.\\
		\bottomrule[1.25pt]
	\end{tabularx}
\end{table}

\subsubsection{Beispiel: WordCount} 

Um Abschließend zu dieser Untersektion das Zusammenspiel zwischen Datensätzen, Transformationen und Aktionen zu zeigen, ist in \autoref{lst:wordcount_beispiel} eine lauffähige Beispielapplikation implementiert, welche eine Textdatei einliest und vorkommenden Wörter (ohne Zahlen und Zeichen) zählt und anschließend die Schlüssel-Werte-Paare, in diesem Fall das jeweilige Wort und dessen Häufigkeit, ausgibt. 

\begin{lstlisting}[language=Java, numbers=left, caption={WordCount-Beispiel}, label={lst:wordcount_beispiel}, float=ht, escapechar=\&]
// Applikationsinitialisierung siehe &\autoref{lst:initialisierung_einer_spark_applikation}&

// Laden der Daten
JavaRDD<String> lines = context.textFile(path);
// Transformationen
lines = lines.map(line -> line.toLowerCase());
lines = lines.map(line -> line.replaceAll("[^a-zäöüß]", " "));
JavaRDD<String> words = lines.flatMap(line -> Arrays.asList(line.split(" ")));
JavaPairRDD<String, Integer> wordCounts = words.mapToPair(word -> new Tuple2<String, Integer>(word, 1));
// Aktionen
wordCounts = wordCounts.reduceByKey((i1, i2) -> i1 + i2);
wordCounts.foreach(wordCount -> System.out.println(wordCount));
\end{lstlisting}

\clearpage

\subsection{Spark SQL}

Spark SQL ist ein Modul, welches insbesondere dafür geeignet ist, strukturierte Daten zu verarbeiten und zu analysieren. Das Modul kann vielseitig verwendet werden. So ist es möglich, simple \gls{sql}- und HiveQL\footnote{HiveQL ist eine Abwandlung von \gls{sql}. Diese entspricht nicht den \gls{sql}-Standards. Für weitere Informationen sei auf das Apache Hive Projekt (\url{http://hive.apache.org}) verwiesen.}-Abfragen entweder innerhalb einer Applikation oder über eine Kommandozeile zu tätigen, oder aber auch die in Spark SQL enthaltenen DataFrame- und Datasets-\glspl{api} zu verwenden \cite{ASFnodateg}.

In \autoref{lst:initialisierung_einer_spark_applikation_mit_sql} ist die Initialisierung einer Spark-Applikation für die Verwendung von Spark SQL dargestellt. Diese ähnelt sehr der Initialisierung einer einfachen Spark"=Applikation aus \autoref{lst:initialisierung_einer_spark_applikation}.

\begin{lstlisting}[language=Java, numbers=left, caption={Initialisierung einer Spark-Applikation für die Verwendung von Spark SQL}, label={lst:initialisierung_einer_spark_applikation_mit_sql}, float=ht, escapechar=\&]
// siehe &\autoref{lst:initialisierung_einer_spark_applikation}&
JavaSparkContext context = new JavaSparkContext(configuration);
SQLContext sqlContext = new SQLContext(context);
\end{lstlisting}

\subsubsection{DataFrames}

DataFrames (engl. \textit{Datenrahmen}) sind das Pendant einer Tabelle in einem \gls{rdbms} und besteht aus mit einem Namen bezeichneten Spalten. Die Erzeugung von DataFrames ist vielseitig. Sie können aus (strukturierten) Dateien, Tabellen aus externen Datenbanken oder aus \glspl{rdd} erstellt werden. In \autoref{lst:erzeugung_von_dataframes} sind hierfür Beispiele gegeben.

\begin{lstlisting}[language=Java, numbers=left, caption={Erzeugung von DataFrames}, label={lst:erzeugung_von_dataframes}, float=ht, escapechar=\&]
// Erzeugung aus einer &\gls{json}&-Datei
DataFrame df1 = sqlContext.read().json(path);

// Erzeugung über &\gls{jdbc}&
Properties properties = new Properties();
// properties.put(..., ...);
DataFrame df2 = sqlContext.read().jdbc(url, tableName, properties);

// Erzeugung aus einer &\gls{rdd}& ("Foo" ist eine JavaBean)
DataFrame df3 = sqlContext.createDataFrame(rdd, Foo.class);
\end{lstlisting}

Wie bei \glspl{rdd} ist es auch möglich, auf DataFrames Transformationen und Aktionen anzuwenden. Eine Auswahl an möglichen Transformationen und Aktionen ist in \autoref{tab:auf_df_anwendbare_trans_akts} aufgelistet. Für eine vollständige Übersicht sei auf die \gls{api}-Dokumentation von Spark verwiesen.

\begin{table}[ht]
	\caption{Auf DataFrames anwendbare Transformationen und Aktionen}
	\label{tab:auf_df_anwendbare_trans_akts}
	\begin{tabularx}{\textwidth}{lX}
		\toprule[1.5pt]
		\textbf{Transformation/Aktion} & \textbf{Bedeutung} \\
		\toprule[1.5pt]
		\lstinline|printSchema| & Gibt das Schema des DataFrames auf der Konsole aus.\\
		\midrule
		\lstinline|select| & Erzeugt einen neuen Datenrahmen, der nur bestimmte Spalten beinhaltet.\\
		\midrule
		\lstinline|show| & Gibt den Inhalt des DataFrames auf der Konsole aus.\\
		\midrule
		\lstinline|groupBy| & Gruppiert die Daten anhand bestimmter Spalten.\\
		\midrule
		\lstinline|javaRDD| & Erzeugt ein \gls{rdd} aus dem DataFrame.\\
		\bottomrule[1.25pt]		
	\end{tabularx}
\end{table}

\subsubsection{Datasets}

Seit Version 1.6 gibt es eine weitere, mittlerweile noch als experimentell gekennzeichnete, \gls{api}, die sogenannte Datasets-\gls{api}. Ziel dieser soll es sein, die möglichen Transformationen und Aktionen von \glspl{rdd} und DataFrames in einer Datenstruktur zu vereinen. So ist es zukünftig nicht mehr nötig, DataFrames in \glspl{rdd} umzuwandeln, um beispielsweise eine \lstinline|map|-Transformation (siehe \autoref{tab:auf_rdds_anwendbare_transformationen}), welche von der DataFrames-\gls{api} nicht unterstützt wird, auf die Daten anzuwenden. In \autoref{lst:erzeugung_von_datasets} wird beispielhaft gezeigt, wie solche Datasets erzeugt werden können.

\begin{lstlisting}[language=Java, numbers=left, caption={Erzeugung von Datasets}, label={lst:erzeugung_von_datasets}, float=ht, escapechar=\&]
// Erzeugung aus einer List<Foo> data1 ("Foo" ist eine JavaBean)
DataSet<Foo> ds1 = sqlContext.createDataset(data1, Encoders.bean(Foo.class));

// Erzeugung aus einer List<String> data2
DataSet<String> ds2 = sqlContext.createDataset(data2, Encoders.STRING());

// Erzeugung aus einer List<Tuple2<Integer, Integer>> data3
Encoder<Tuple2<Integer, String>> encoder = Encoders.tuple(Encoders.INT(), Encoders.STRING());
DataSet<Tuple2<Integer, String>> ds2 = sqlContext.createDataset(data3, encoder);
\end{lstlisting}

\subsection{Spark Streaming}

Spark Streaming ist ein weiteres Modul, welches die für Verarbeitung von Echtzeitdaten zuständig ist. So können Streams zu unterschiedlichen Datenquellen hergestellt und die empfangenen Daten anschließend weiterverarbeitet werden \cite{ASFnodateh}. Dabei können die gleichen Transformationen und Aktionen auf die Daten bzw. Datensätze angewendet werden, wie bei \glspl{rdd} (siehe \autoref{tab:auf_rdds_anwendbare_transformationen} und \autoref{tab:auf_rdds_anwendbare_aktionen}). Zu den Datenquellen, zu denen Streams hergestellt werden können, gehören u. a. die Folgenden:

\begin{itemize}
	\item Kafka (\url{http://kafka.apache.org/})
	\item Flume (\url{http://flume.apache.org/})
	\item \gls{hdfs}
	\item Amazon S3 (\url{https://aws.amazon.com/de/s3/})
	\item Amazon Kinesis (\url{https://aws.amazon.com/de/kinesis/})
	\item Twitter (\url{https://twitter.com/}) und
	\item \gls{tcp}-Sockets
\end{itemize}

Um Spark Streaming in einer Applikation zu verwenden, wird auch hier eine Initialisierung des Moduls benötigt. Das Vorgehen ist analog zu Spark \gls{sql} und ist in \autoref{lst:initialisierung_einer_spark_applikation_mit_streaming} dargestellt.

\begin{lstlisting}[language=Java, numbers=left, caption={Initialisierung einer Spark-Applikation für die Verwendung von Spark Streaming}, label={lst:initialisierung_einer_spark_applikation_mit_streaming}, float=ht, escapechar=\&]
// siehe &\autoref{lst:initialisierung_einer_spark_applikation}&
JavaSparkContext context = new JavaSparkContext(configuration);
JavaStreamingContext streamingContext = new JavaStreamingContext(context, Durations.Seconds(2));
\end{lstlisting}

\subsubsection{\acrshort{dstream}}

Ein \gls{dstream} ist das Pendant zu dem gewöhnlichen \gls{rdd} und kann genau so wie dieses behandelt werden. Im Allgemeinen sind wie bereits erwähnt alle Transformationen und Aktionen, die auch zur Bearbeitung von einem \gls{rdd} verfügbar sind, und einige zusätzliche Funktionen auf einen solchen Stream anwendbar. Um die Streams zu erzeugen, werden auch hier Methoden angeboten, um Daten aus dem lokalen Dateisystem, einem \gls{hdfs} oder einem \gls{tcp}-Socket als Quelle zu verwenden. Zusätzlich sind auch Erweiterungen verfügbar, um z. B. Twitter-Inhalte (Tweets) als Quelle zu verwenden. Einige Beispiele hierfür sind in \autoref{lst:erzeugung_von_dstreams} aufgeführt.

\begin{lstlisting}[language=Java, numbers=left, caption={Erzeugung von DStreams}, label={lst:erzeugung_von_dstreams}, float=ht, escapechar=\&]
// DStream, welcher Daten aus einem &\gls{hdfs}& als Quelle verwendet
JavaDStream<String> ds1 = streamingContext.textFileStream("hdfs://localhost:9000/data");

// DStream, welcher Daten aus einem &\gls{tcp}&-Socket als Quelle verwendet
JavaReceiverInputDStream<String> ds2 = streamingContext.socketTextStream("localhost", 1234);

// DStream, welcher Daten aus Twitter als Quelle verwendet (Status ist 
// die Repräsentation eines Tweets und TwitterUtils eine Hilfsklasse, 
// welche von der Twitter-Erweiterung für Spark Streaming enthalten ist)
JavaReceiverInputDStream<Status> ds3 = TwitterUtils.createStream(streamingContext);
\end{lstlisting}

\subsubsection{Beispiel: WordCount mit Spark Streaming}

Auch diese Untersektion soll mit einem Beispiel abgeschlossen werden. Es soll die Zusammenhänge zwischen \glspl{rdd} und \glspl{dstream} aufzeigen. So wieder hier das in \autoref{lst:wordcount_beispiel} gezeigte Beispiel abgewandelt und es wird anstatt einer Datei ein (\gls{tcp})-Stream eingelesen und auf die gleiche Weise die vorkommenden Wörter in einem Intervall, welches wie in Zeile 3 aus \autoref{lst:initialisierung_einer_spark_applikation_mit_streaming} festgelegt wird, aufgezählt und ausgegeben.

\begin{lstlisting}[language=Java, numbers=left, caption={WordCount-Beispiel mit Spark Streaming}, label={lst:wordcount_beispiel_mit_streaming}, float=ht, escapechar=\&]
// Applikationsinitialisierung siehe &\autoref{lst:initialisierung_einer_spark_applikation_mit_streaming}&

// Erzeugen des Streams
JavaReceiverInputDStream<String> stream = streamingContext.socketTextStream("localhost", 1234);
// Transformationen
JavaDStream<String> lines = stream.map(line -> line.toLowerCase());
lines = lines.map(line -> line.replaceAll("[^a-zäöüß]", " "));
JavaDStream<String> words = lines.flatMap(line -> Arrays.asList(line.split(" ")));
JavaPairDStream<String, Integer> wordCounts = words.mapToPair(word -> new Tuple2<String, Integer>(word, 1));
// Aktionen
wordCounts = wordCounts.reduceByKey((i1, i2) -> i1 + i2);
wordCounts.foreachRDD(rdd -> rdd.foreach(wordCount -> System.out.println(wordCount)));
\end{lstlisting}

\subsection{MLlib}

Die \gls{mllib} ist eine Erweiterung von Spark, welche sich dem maschinellen Lernen (engl. \textit{Machine Learning}) widmet, implementiert hierfür mehrere Algorithmen und stellt weiter Werkzeuge bereit, mit welchen sowohl auf Low- als auch auf High-Level-Ebene gearbeitet werden kann \cite{ASFnodatei}. 

Bei der \gls{mllib} handelt es sich um zwei eigentlich getrennten Bibliotheken. Hierbei gibt es einerseits eine Bibliothek, welche Werkzeuge auf Low-Level-Ebene bietet und auf \glspl{rdd} arbeitet, und andererseits eine als bevorzugt gekennzeichnete Bibliothek, welche Werkzeuge auf High-Level-Ebene bietet und auf DataFrames arbeitet.

Auf eine detailliertere Beschreibung der Benutzungsvorgehensweise und der einzelnen Algorithmen und Werkzeuge wird in dieser Arbeit verzichtet, da dies von dem eigentlichen Thema abweichen und über den Rahmen hinausgehen würde. Jedoch wird in den Listings von \autoref{anwendungsbeispiel} Gebrauch von \gls{mllib} gemacht. Für weitere Informationen auf die Dokumentation der Komponente \cite{ASFnodatei} verwiesen. 

\subsection{GraphX}

GraphX ist eine Erweiterung der \glspl{rdd} und bietet eine Abstraktion für Graphen an. Mit einer Sammlung von fundamentalen Operatoren und der Implementierung mehrerer Algorithmen, welche auf Graphen arbeiten, ermöglicht GraphX die Verarbeitung und Berechnung von Graphen \cite{ASFnodatej}.

Auch bei GraphX wird auf eine genauere Beschreibung der Benutzungsvorgehensweise und der einzelnen Algorithmen und Werkzeuge verzsichtet. Es sei weiterhin auf die spezielle Dokumentation \cite{ASFnodatej} verwiesen.

\section{Einsatzgebiete}

Aus einer im Jahr 2015 durchgeführten Umfrage von \citeauthor{Databricks2015a} zum Thema Spark geht hervor, dass zahlreiche unterschiedliche Branchen das Framework Apache Spark zum Einsatz bringen \cite{Databricks2015a}. Die einzelnen Branchen, die sich, im Bezug auf den Einsatz von Apache Spark, aus der Umfrage hervorgehoben haben, sind in \autoref{abb:bedeutendste_branchen_mit_einsatz_von_apache_spark} dargestellt.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{bedeutendste_branchen_mit_einsatz_von_apache_spark.eps}
	\caption[Bedeutendste Branchen mit dem Einsatz von Apache Spark]{Bedeutendste Branchen mit dem Einsatz von Apache Spark}
	\label{abb:bedeutendste_branchen_mit_einsatz_von_apache_spark}
\end{figure}

Aus der genannten Umfrage geht außerdem hervor, dass Apache Spark zur Lösung von unterschiedlichen Problemen innerhalb der Branchen eingesetzt wird und so mit Hilfe des Frameworks verschiedene Arten an Produkten hergestellt werden. Hierbei ist eine starke Variation erkennbar und die Themenfelder decken Gebiete wie \gls{bi}, Log-Verarbeitung, aber auch Sicherheitssysteme ab. Die Umfrageergebnisse sind in \autoref{abb:mit_apache_spark_erstellte_produkte} visualisiert. 

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{mit_apache_spark_erstellte_produkte.eps}
	\caption[Mit Apache Spark erstellte Produkte]{Mit Apache Spark erstellte Produkte}
	\label{abb:mit_apache_spark_erstellte_produkte}
\end{figure}

Auch die Apache Spark einsetzenden Berufsgruppen unterscheiden sich. So gibt es u. a. Data Engineers, Data Scients, aber auch Personen aus dem Management und Akademiker, die Apache Spark einsetzen. Die einzelnen Ergebnisse der Umfrage sind in \autoref{abb:mit_apache_spark_verbundene_berufsgruppen} einzeln dargestellt.

\begin{figure}[ht]
	\centering
	\includegraphics[width=0.8\textwidth]{mit_apache_spark_verbundene_berufsgruppen.eps}
	\caption[Mit Apache Spark verbundene Berufsgruppen]{Mit Apache Spark verbundene Berufsgruppen}
	\label{abb:mit_apache_spark_verbundene_berufsgruppen}
\end{figure}

Insgesamt lässt sich aus den einzeln untersuchten Umfrageergebnisse schließen, dass Spark nicht nur im reinen Big Data Segment eingesetzt werden kann bzw. wird, sondern vielmehr auch in unterschiedlichen Bereichen, welche nicht direkt mit Big Data in Verbindung gebracht werden. Somit kann Apache Spark schließlich als allgemein und flexibel einsetzbares Framework für Datenverarbeitung bezeichnet werden.