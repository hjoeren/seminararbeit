\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{docbase}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{scrreprt}}
\ProcessOptions\relax
\LoadClass{scrreprt}

\KOMAoption{fontsize}{11pt}
\KOMAoption{parskip}{full}
\KOMAoption{numbers}{noenddot}
\KOMAoption{listof}{totoc}
\KOMAoption{bibliography}{totoc}

\addtokomafont{caption}{\small}

\setkomafont{pageheadfoot}{\small\rmfamily\mdseries}
\setkomafont{pagenumber}{\small\rmfamily\mdseries}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\RequirePackage{lmodern}

\RequirePackage[english,ngerman]{babel}

\RequirePackage[labelfont=bf]{caption}

\RequirePackage[headsepline]{scrpage2}
\automark[chapter]{section}
\clearscrheadings
\ihead{\headmark}
\cfoot{\pagemark}

\RequirePackage{chngcntr}
\counterwithout{footnote}{chapter}

\RequirePackage{tabularx}
\RequirePackage{booktabs}

\RequirePackage{xcolor}

\RequirePackage{graphicx}
\graphicspath{{./assets/img/}}

\RequirePackage{pifont}

\RequirePackage[outdir=./build/]{epstopdf}

\RequirePackage{url}
\RequirePackage{hyperref}
\hypersetup{
	pdfdisplaydoctitle = true,
	pdfstartview = Fit,
	colorlinks = true,
	allcolors = black,
	urlcolor = blue
}

\RequirePackage{pdfpages}

\RequirePackage[acronym, nomain, nonumberlist, nopostdot, toc]{glossaries}
\let\acrlonggen\glsuseri
\let\acrlongdat\glsuserii
\makeglossaries

\RequirePackage{listings}
\RequirePackage{scrhack}
\lstdefinestyle{customstyle}{
	basicstyle = \ttfamily\footnotesize,
	keywordstyle = \color[rgb]{0,0,1},
	stringstyle = \color[rgb]{0.64,0.08,0.08},
	commentstyle = \color[rgb]{0,0.5,0},
	rulecolor = \color[rgb]{0,0,0},
	tabsize = 2,
	showstringspaces = true,
	numbers = none,
	frame = single,
	float = ht,
	breaklines = true,
	literate = {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
	{Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
	{à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
	{À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
	{ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
	{Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
	{â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
	{Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
	{œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
	{ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
	{ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
	{€}{{\EUR}}1 {£}{{\pounds}}1
}
\lstdefinestyle{customxmlstyle}{
	basicstyle = \ttfamily\footnotesize,
	keywordstyle = \color[rgb]{0,0,0},
	stringstyle = \color[rgb]{0,0,0},
	commentstyle = \color[rgb]{0,0,0},
	rulecolor = \color[rgb]{0,0,0},
	tabsize = 2,
	showstringspaces = false,
	numbers = none,
	frame = single,
	float = !h,
	breaklines = true,
	literate = {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
	{Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
	{à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
	{À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
	{ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
	{Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
	{â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
	{Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
	{œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
	{ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
	{ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
	{€}{{\EUR}}1 {£}{{\pounds}}1
}
\lstset{style = customstyle, inputpath = ./assets/lst/}
\renewcommand{\lstlistlistingname}{Listingverzeichnis}
\providecommand*{\listingautorefname}{Listing}

\RequirePackage[babel, german = quotes]{csquotes}
\RequirePackage[backend = biber, style = alphabetic-verb]{biblatex}
\ExecuteBibliographyOptions{
	bibencoding = utf8,
	bibwarn = true
}
\renewcommand*{\labelalphaothers}{}
\DefineBibliographyStrings{ngerman}{
	bibliography = {Literaturverzeichnis},
	andothers = {et\addabbrvspace al\adddot}
}
\setcounter{biburllcpenalty}{7000}
\setcounter{biburlucpenalty}{8000}

\RequirePackage[titletoc]{appendix}
\noappendicestocpagenum
\newcommand*{\Anhangautorefname}{Anhang}

\IfFileExists{assets/bib/bibliography.bib} {
	\addbibresource{assets/bib/bibliography.bib}
}{
	\ClassWarning{docbase}{assets/bib/bibliography.bib does not exist}
}

\IfFileExists{00_0_abbreviations} {
	\input{00_0_abbreviations}
}{
	\ClassWarning{docbase}{00_0_abbreviations.tex does not exist}
}

\AtBeginDocument{
	\ClassWarning{docbase}{Path of lock flag: ./00_1_lock_flag.tex}
	\ClassWarning{docbase}{Path of statutory declaration: ./00_2_statutory_declaration}
	\ClassWarning{docbase}{Path of abstracts: ./00_3_abstract_de.tex and 00_3_abstract_en.tex}
	\ClassWarning{docbase}{Path of apenndix: ./00_3_abstract_de.tex and 00_appendix.tex}
	\ClassWarning{docbase}{Path of images: ./assets/img/}
	\ClassWarning{docbase}{Path of listings: ./assets/lst/}
	
	\pagestyle{empty}
	\pagenumbering{gobble}
	
	\maketitle
	
	\IfFileExists{00_1_lock_flag}{
		\chapter*{Sperrvermerk}
		\input{00_1_lock_flag}
	}{
		\ClassWarning{docbase}{00_1_lock_flag.tex does not exist}
	}
	\IfFileExists{00_2_statutory_declaration}{
		\chapter*{Eidesstattliche Erklärung}
		\input{00_2_statutory_declaration}	
	}{
		\ClassWarning{docbase}{00_2_statutory_declaration.tex does not exist}
	}
	\IfFileExists{00_3_abstract_de}{
		\begin{abstract}
      \input{00_3_abstract_de}
    \end{abstract}
	}{
		\ClassWarning{docbase}{00_3_abstract_de.tex does not exist}	
	}
	\IfFileExists{00_3_abstract_en}{
    \begin{otherlanguage}{english}
      \begin{abstract}
        \input{00_3_abstract_en}
      \end{abstract}
    \end{otherlanguage}
	}{
		\ClassWarning{docbase}{00_3_abstract_en.tex does not exist}	
	}
	
	\clearpage
	\pagestyle{plain}
	\pagenumbering{Roman}
	
	\pdfbookmark{\contentsname}{toc}
	\tableofcontents
	\listoffigures
	\listoftables
	\lstlistoflistings
	\printacronyms[title=Abkürzungsverzeichnis]
	
	\clearpage
	\pagestyle{scrheadings}
	\pagenumbering{arabic}
}

\AtEndDocument{
	\printbibliography
	\IfFileExists{00_appendix}{
		\begin{appendices}
			\input{00_appendix}
		\end{appendices}
	}{
		\ClassWarning{docbase}{00_appendix.tex does not exist}	
	}
}

\endinput

\usepackage{cleveref}