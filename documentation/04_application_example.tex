\chapter{Anwendungsbeispiel: Sentiment-Analyse von Tweets}
\label{anwendungsbeispiel}

Das folgende Kapitel beschreibt die Java-Implementierung eines etwas komplizierterem Anwendungsfall. Ziel der Implementierung soll es sein, eine Sentiment-Analyse von Tweets zu machen, und dabei in einem Intervall von einer bestimmten Anzahl an Sekunden Tweets zu einem bestimmten Thema, welches durch die Angabe von Schlagwörtern (\enquote{Hashtags}) festgelegt werden kann. Zum Einsatz kommen neben der zentralen Kernkomponente noch Spark Streaming für das Abgreifen der Tweets und Spark \gls{sql} und \gls{mllib} für die Bewertung bzw. Analyse der Inhalte. Das Beispiel basiert auf einem Scala-Beispiel aus dem Werk \citetitle{Guller2015} von \citeauthor{Guller2015} \cite[Kapitel 8]{Guller2015}.

Die gesamte Implementierung der Applikation ist in \autoref{app:anwendungsbeispiel} zu finden. Hierbei wird in diesem Kapitel lediglich die einzelnen wichtigsten Schritte beschrieben und auf die Implementierung im Anhang verwiesen.

\section{Applikationsinitialisierung}

Wie in \autoref{sek:komponenten} bereits erwähnt, wird eine Initialisierung benötigt, um auf die Komponenten von Spark zuzugreifen. Hierzu werden die einzelnen Kontexte analog zur Beschreibung der Komponenten erzeugt, was in den Zeilen 233-242 des \autoref{lst:twitter_sentiment_analysis_java} erfolgt. Die einzelnen Kontexte werden im Anschluss an die jeweiligen Methoden weitergegeben und dort verwendet.

\section{Modellerstellung}

Zur Erstellung eines Modells, welches für die Analyse der einzelnen Tweets benötigt wird, muss ein Textkorpus vorliegen. Dieser beinhaltet eine Sammlung bereits bewerteter Tweets. In diesem Beispiel ist dieser von \citeauthor{Sentiment140nodate} bezogen \cite{Sentiment140nodate}, welcher 1.600.000 Tweets mit Bewertung der Sentimente beinhaltet. Das genaue Format ist wie folgt:

\begin{description}
	\item[Bewertung] des Tweets (0 für negativ, 2 für neutral oder 4 für positiv)
	\item[ID] des Tweets (kann vernachlässigt werden)
	\item[Datum] des Tweets (kann vernachlässigt werden)
	\item[Abfrage] des Tweets (kann vernachlässigt werden)
	\item[Benutzer] des Tweets (kann vernachlässigt werden)
	\item[Text] des Tweets
\end{description}

Somit ergeben sich folgende Beispieldaten:

\begin{lstlisting}[numbers = left, float=ht]
"0","1234567890","Fri Jun 05 22:51:00 PDT 2009","NO_QUERY","haagjoeren","This is a negative tweet"
"2","1234567891","Fri Jun 05 22:52:00 PDT 2009","NO_QUERY","haagjoeren","This is a neutral tweet"
"4","1234567892","Fri Jun 05 22:53:00 PDT 2009","NO_QUERY","haagjoeren","This is a positive tweet"
\end{lstlisting}

Der Korpus enthält bis hierher noch irrelevante Teile, welche, wie in der Beschreibung des Formats erwähnt, vernachlässigt werden können. Deshalb werden Transformationen auf die Datensätze angewendet, sodass der Korpus lediglich Texte und deren Bewertungen (abgeändert zu 0 für negativ und 1 für positiv) enthält und in einem DataFrame verwaltet wird. Dies geschieht in den Zeilen 132-156 des \autoref{lst:twitter_sentiment_analysis_java} unter Einbezug der JavaBean-Klasse \lstinline|LabelledTweet| (siehe \autoref{lst:labelled_tweet_java}). Prinzipiell sieht der Inhalt der DataFrames aus wie folgt:

\begin{lstlisting}[numbers = left, float=ht, escapechar=\&]
"This is a negative tweet",0
&\textcolor{lightgray}{\textit{"This is a neutral tweet",-1}}&
"This is a positive tweet",1
\end{lstlisting}

Für das weitere Vorgehen zur Erstellung des Modells (im Allgemeinen die Methode \lstinline|getOrCreateModel| aus \autoref{lst:twitter_sentiment_analysis_java}) sei auf das Ursprungsbeispiel verweisen. Hier sind die weiteren Transformationen, Zwischenschritte und der angewendete Algorithmus aus der \gls{mllib}-Komponente detaillierter beschrieben.

\section{Streaming der Tweets}

Für das Streaming der Tweets (siehe Zeile 199-216 des \autoref{lst:twitter_sentiment_analysis_java}) wird Gebrauch von der Spark Streaming Erweiterung für Twitter gemacht. Dabei kommen für das Erzeugen des Streams Daten für die Autorisierung bei Twitter und Filter, welche die Hashtags beinhalten, zum Einsatz. Beide werden durch die Konfigurationsdatei (siehe \autoref{lst:application_properties}) festgelegt. Durch die Angabe der Filter wird eine Transformation vorgeschaltet und der Stream beinhaltet nur Tweets, die im Zusammenhang mit den angegebenen Hashtags stehen. Anschließend werden zwei weitere \lstinline|filter|-Transformationen auf den bestehend Stream angewendet, sodass nur echte Tweets (und keine Retweets) und Tweets der durch die Konfigurationsdatei festgelegten Sprachen berücksichtigt werden. Um die Tweets anschließend mit dem zuvor erstellen Modell zu analysieren, werden sie mit einer \lstinline|map|-Transformation umgewandelt, sodass Objekte des Typs \lstinline|Tweet| (siehe \autoref{lst:tweet_java}) vorliegen.

\section{Analyse der Tweets}

Für die Analyse der Tweets (siehe Zeile 218-228 des \autoref{lst:twitter_sentiment_analysis_java}) wird aus jedem \gls{rdd} des Streams zuerst ein DataFrame mit der Stuktur der \lstinline|Tweet|-Klasse erzeugt. Dieser wird anschließend mit Hilfe des zuvor erzeugten Modells zu einem weiteren DataFrame transformiert, sodass eine Spalte \enquote{prediction} angehängt wird, welche die Bewertung der Tweet-Texte beinhaltet. Die Spalten "`text"' und die eben erwähnte Spalte werden ausgewählt, wodurch in einem Zwischenschritt ein weiterer DataFrame entsteht. Dieser wird in ein \gls{rdd} umgewandelt, sodass ein Schlüssel-Werte-Paar vorliegt. Die Form des soeben erzeugten \glspl{rdd} hat nun die folgende Form:

\begin{lstlisting}
(Tweet-Text, Bewertung)
\end{lstlisting}

Diese erzeugten \glspl{rdd} werden schließlich auf einem in der Konfigurationsdatei festgelegten Speicherort (lokal, \gls{hdfs}, \dots) abgespeichert.