\NeedsTeXFormat{LaTeX2e}
\ProvidesClass{prebase}

\DeclareOption*{\PassOptionsToClass{\CurrentOption}{beamer}}
\ProcessOptions\relax
\LoadClass{beamer}

\definecolor{hskared}{rgb}{0.929,0.11,0.142}
\usetheme{Berlin}
\usecolortheme[named=hskared]{structure}
%\usecolortheme{beaver}

\newcommand*\oldmacro{}
\let\oldmacro\insertshorttitle
\renewcommand*\insertshorttitle{
	\oldmacro\hfill
	\insertframenumber\,/\,\inserttotalframenumber
}

\RequirePackage[utf8]{inputenc}
\RequirePackage[T1]{fontenc}

\RequirePackage{lmodern}

\RequirePackage[english,ngerman]{babel}

\RequirePackage[labelfont=bf]{caption}

\RequirePackage{tabularx}
\RequirePackage{booktabs}

\RequirePackage{xcolor}

\RequirePackage{graphicx}
\graphicspath{{./assets/img/}}

\RequirePackage{pifont}

\RequirePackage[outdir=./build/]{epstopdf}

\RequirePackage{url}
\RequirePackage{hyperref}
\hypersetup{
	pdfdisplaydoctitle = true,
	pdfstartview = Fit,
	urlcolor = blue
}

\RequirePackage{pdfpages}

\RequirePackage{listings}
\RequirePackage{scrhack}
\lstdefinestyle{customstyle}{
	basicstyle = \ttfamily\footnotesize,
	keywordstyle = \color[rgb]{0,0,1},
	stringstyle = \color[rgb]{0.64,0.08,0.08},
	commentstyle = \color[rgb]{0,0.5,0},
	rulecolor = \color[rgb]{0,0,0},
	tabsize = 2,
	showstringspaces = true,
	numbers = none,
	frame = single,
	float = ht,
	breaklines = true,
	literate = {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
	{Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
	{à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
	{À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
	{ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
	{Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
	{â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
	{Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
	{œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
	{ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
	{ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
	{€}{{\EUR}}1 {£}{{\pounds}}1
}
\lstdefinestyle{customxmlstyle}{
	basicstyle = \ttfamily\footnotesize,
	keywordstyle = \color[rgb]{0,0,0},
	stringstyle = \color[rgb]{0,0,0},
	commentstyle = \color[rgb]{0,0,0},
	rulecolor = \color[rgb]{0,0,0},
	tabsize = 2,
	showstringspaces = false,
	numbers = none,
	frame = single,
	float = !h,
	breaklines = true,
	literate = {á}{{\'a}}1 {é}{{\'e}}1 {í}{{\'i}}1 {ó}{{\'o}}1 {ú}{{\'u}}1
	{Á}{{\'A}}1 {É}{{\'E}}1 {Í}{{\'I}}1 {Ó}{{\'O}}1 {Ú}{{\'U}}1
	{à}{{\`a}}1 {è}{{\`e}}1 {ì}{{\`i}}1 {ò}{{\`o}}1 {ù}{{\`u}}1
	{À}{{\`A}}1 {È}{{\'E}}1 {Ì}{{\`I}}1 {Ò}{{\`O}}1 {Ù}{{\`U}}1
	{ä}{{\"a}}1 {ë}{{\"e}}1 {ï}{{\"i}}1 {ö}{{\"o}}1 {ü}{{\"u}}1
	{Ä}{{\"A}}1 {Ë}{{\"E}}1 {Ï}{{\"I}}1 {Ö}{{\"O}}1 {Ü}{{\"U}}1
	{â}{{\^a}}1 {ê}{{\^e}}1 {î}{{\^i}}1 {ô}{{\^o}}1 {û}{{\^u}}1
	{Â}{{\^A}}1 {Ê}{{\^E}}1 {Î}{{\^I}}1 {Ô}{{\^O}}1 {Û}{{\^U}}1
	{œ}{{\oe}}1 {Œ}{{\OE}}1 {æ}{{\ae}}1 {Æ}{{\AE}}1 {ß}{{\ss}}1
	{ű}{{\H{u}}}1 {Ű}{{\H{U}}}1 {ő}{{\H{o}}}1 {Ő}{{\H{O}}}1
	{ç}{{\c c}}1 {Ç}{{\c C}}1 {ø}{{\o}}1 {å}{{\r a}}1 {Å}{{\r A}}1
	{€}{{\EUR}}1 {£}{{\pounds}}1
}
\lstset{style = customstyle, inputpath = ./assets/lst/}
\providecommand*{\listingautorefname}{Listing}

\RequirePackage[babel, german = quotes]{csquotes}
\RequirePackage[backend = biber, style = numeric-comp]{biblatex}
\ExecuteBibliographyOptions{
	bibencoding = utf8,
	bibwarn = true
}
\renewcommand*{\labelalphaothers}{}
\DefineBibliographyStrings{ngerman}{
	bibliography = {Literaturverzeichnis},
	andothers = {et\addabbrvspace al\adddot}
}
\setcounter{biburllcpenalty}{7000}
\setcounter{biburlucpenalty}{8000}

\IfFileExists{assets/bib/bibliography.bib} {
	\addbibresource{assets/bib/bibliography.bib}
}{
\ClassWarning{docbase}{assets/bib/bibliography.bib does not exist}
}

\AtBeginSection[]{
	\begin{frame}[plain,noframenumbering]{Agenda}
		\setcounter{tocdepth}{1}
		\tableofcontents[currentsection]
	\end{frame}
}

\AtBeginDocument{
	\ClassWarning{docbase}{Path of images: ./assets/img/}
	\ClassWarning{docbase}{Path of listings: ./assets/lst/}
	
	\begin{frame}[plain,noframenumbering]
		\titlepage
	\end{frame}
	
	\begin{frame}[plain,noframenumbering]{Agenda}
		\tableofcontents
	\end{frame}
}

\AtEndDocument{
	%\section{}
	%\begin{frame}[plain,noframenumbering]
	%	\centering
	%	Vielen Danke für Ihre Aufmerksamkeit!
	%\end{frame}
	
	\IfFileExists{00_appendix}{
		\section{}
		\begin{frame}[plain,noframenumbering]
		\end{frame}
		\input{00_appendix}
	}{
	\ClassWarning{docbase}{00_appendix.tex does not exist}	
	}
}

\endinput

\usepackage{cleveref}